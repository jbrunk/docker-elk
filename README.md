# ELK

A simple ELK Installation.

All Docker Services / Ports are mapped to the physical Host (eg. 0.0.0.0:9200).

Therefore if you'll use boot2docker on OSX you have to change `http://localhost` in `elasticsearch/files/elasticsearch.yml` (and all Examples below) to the IP of your boot2docker Virtualbox Instance (boot2docker ip).

## Components

* Oracle Java 1.7.71
* Logstash 1.4.2
* Kibana 3.1.2
* ElasticSearch 1.4.1

## Requirements

* Docker (see: https://docs.docker.com/installation/#installation)
* Fig (see: http://www.fig.sh/install.html)

## Architecture

[client] => [logstash] => [elasticsearch] <= [kibana]

## Containers

|Container|Desc.|Ports|
|---|---|---|
|elasticsearch|ElasticSearch Instance|0.0.0.0:9200/tcp|
|logstash|Logstash Indexer|Text:0.0.0.0:1514/udp+tcp Syslog:0.0.0.0:514/udp+tcp|
|kibana|Kibana Web Frontend|0.0.0.0:80/tcp|
|client|Basic Host with rsylog => logstash|-|

## Start

After installing Docker and Fig (aehm yes you'll need a git client too) ...

```
$ git clone https://bitbucket.org/jbrunk/docker-elk.git
$ cd docker-elk
$ fig up -d
```

## Kibana Web Frontend

http://localhost/index.html#/dashboard/file/logstash.json

## ElasticSearch (EL)

http://localhost:9200/

## EL Marvel 

http://localhost:9200/_plugin/marvel

## samples

send normal text to logstash:

```
$ echo "Hello World!" | nc localhost 1514

```

send syslog message to logstash:

```
$ docker exec -i -t elk_client_1 bash
# logger client-test
```

